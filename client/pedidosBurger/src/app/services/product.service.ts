import { Extra } from './../interfaces/extra.interface';
import { Product } from '../interfaces/product.interface';
import { Menu} from './../interfaces/menu.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs';
import { Ingredients } from '../interfaces/ingredients.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _productsSelected: Product[] = [];

  get productsSelected(): Product[] {
    return this._productsSelected;
  }
  set productsSelected(value: Product[]) {
    this._productsSelected = value;
  }

  
  private _productSelected: Product[] = [];

  get productSelected(): Product[] {
    return this._productSelected;
  }
  set productSelected(value: Product[]) {
    this._productSelected = value;
  }

  API_URL = 'http://localhost:3000/api';

constructor(private http: HttpClient) { }

getMenu(): Observable<Menu[]>{
  return this.http.get<Menu[]>(`${this.API_URL}/burguers`).pipe(map((res: Menu[])=> res));
}

getProducts(id: Product[]): Observable<Product[]>{
  return this.http.get<Product[]>(`${this.API_URL}/burguers/${id}`).pipe(map((res: any)=> res.data));
}

getProduct(id: Product[]): Observable<Product[]>{
  return this.http.get<Product[]>(`${this.API_URL}/burguers/product/${id}`).pipe(map((res: any)=> res.data[0]));
}

getExtra(id: Extra[]): Observable<Extra[]>{
  return this.http.get<Extra[]>(`${this.API_URL}/burguers/product_extra/${id}`).pipe(map((res: any)=> res.data));
}

getIngredientsPrincipal(id: Ingredients[]): Observable<Ingredients[]>{
  return this.http.get<Ingredients[]>(`${this.API_URL}/burguers/product_ingredientPrincipal/${id}`).pipe(map((res: any)=> res.data));
}

}



