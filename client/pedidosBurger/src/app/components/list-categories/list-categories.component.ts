import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Menu } from 'src/app/interfaces/menu.interface';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.css']
})
export class ListCategoriesComponent implements OnInit {

  public listMenu: Menu[];

  constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.listMenu = [];
  }

  ngOnInit() {
    this.getMenu();
  }

  getMenu() {
    this.productService.getMenu().subscribe(res => {
      this.listMenu = res;
    });
  }

  selectCategory(category: any) {
    this.productService.productsSelected = category.id_category;
  }
}