import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/interfaces/menu.interface';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  listMenu: Menu [] = [];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getMenu();
  }

getMenu(){
  this.productService.getMenu().subscribe(res => {
    this.listMenu = res;
  });
}
}