import { map } from 'rxjs/internal/operators/map';
import { Ingredients } from './../../interfaces/ingredients.interface';
import { Extra } from './../../interfaces/extra.interface';

import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Menu } from 'src/app/interfaces/menu.interface';
import { Product } from 'src/app/interfaces/product.interface';
import { Observable, tap } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public productExtra: Extra[];
  public ingredientPrincipal: Ingredients[];
  public extras: boolean;
  public ingPrincipal: boolean;
  public buttons: boolean;
  public option: boolean;
  public extraSelected: number = 0;
  public id: any;
  public data: any;

  constructor(private productService: ProductService, private router: Router) {
    this.productExtra = [];
    this.ingredientPrincipal = [];
    this.extras = false;
    this.ingPrincipal = false;
    this.option = false;
    this.buttons = true;
    // this.id = this.productOne = this.productService.productSelected;
    this.id = this.productService.productSelected;
  }

  productOne$!: Observable<Product>;
  productExtra$!: Observable<Extra>;

  ngOnInit() {
    this.productOne$ = this.productService.getProduct(this.id).pipe(map((res: any) => res));
    this.productService.getExtra(this.id).subscribe(res => {
      this.productExtra = res;
      if (this.productExtra[0].state_extra == 'active' ) {
        this.extras = true;
      } else {
        this.option = true;
      }
    });

    // this.ingredientsPrincipal(this.id);
  }

  // getExtras() {
    /* 
Mostrar si tiene ingredientes extra
*/
  //   this.productExtra$ = this.productService.getExtra(this.id).pipe(map((res: any) => res));
  //   console.log("@", this.productExtra$)
  //   this.option = true;

  // }

  /* 
  Mostrar los ingredientes principales 
  */
  ingredientsPrincipal(id: any) {
    this.productService.getIngredientsPrincipal(id).subscribe(res => {
      this.ingredientPrincipal = res;
      if (this.ingredientPrincipal[0].id_ingredient) {
      this.ingPrincipal = true;
      this.extras = false;
      }
      console.log(this.ingredientPrincipal);

    });
  }


  hasPrevious() {
    if (this.productExtra) {
      return false;
    }

    return this.extraSelected - 1;
  }

  hasNext() {
    this.ingredientsPrincipal(this.id);
    // this.ingPrincipal = true;
    // this.extras = false;

  }

  prueba() {
    // this.extras = false;
    // this.ingPrincipal = true;
    // this.ingPrincipal = false;
  }


  // hasPrevious() {
  //   if (!this.ingredientPrincipal){
  //     console.log("11")
  //     this.extraSelected = 1; //guardar el valor previamente seleccionado de la opción hacerlo grande
  //     return false;
  //   }
  //   return true;
  // }
}