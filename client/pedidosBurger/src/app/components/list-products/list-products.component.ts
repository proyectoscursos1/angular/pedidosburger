import { Product } from '../../interfaces/product.interface';

import { ProductService } from 'src/app/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/interfaces/menu.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  public listProducts: Product[];
  public id: Product[];

  constructor(private productService: ProductService, private router: Router) {
    this.listProducts = [];
    this.id = this.productService.productsSelected;
  }

  ngOnInit() {
    this.productService.getProducts(this.id).subscribe(res => {
      this.listProducts = res;
      if (!this.listProducts) {
        this.router.navigate(['/list-categories']);
      }
    });
  }

  selectProduct(product: any) {
    this.productService.productSelected = product.id_product;
  }
}
