export interface Extra {
    'id_extra': number,
    'name_extra': string,
    'img_extra': string,
    "price_extra": number,
    "state_extra": string
}
