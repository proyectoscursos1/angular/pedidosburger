export interface Product {
    'id_product': number,
    'name_product': string,
    'img_product': string,
    "price_product": string,
    "id_extras": string,
    "id_category": number,
    "id_extra": number
}