export interface Menu {
    'id_category': number,
    'name_category': string,
    'img_category': string,
}