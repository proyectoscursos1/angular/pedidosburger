export interface Ingredients {
    "id_product": number,
    "id_ingredient": number,
    "name_ingredient": string,
    "img_ingredient": string,
    "price_ingredient": number,
    "state_ingredient": string,
    "selected_ingredient": string,
    "type_ingredient": string
}
