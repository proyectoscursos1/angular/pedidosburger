"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class BurguersController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const burguers = yield database_1.default.query('SELECT * FROM categories');
            res.json(burguers);
        });
    }
    getProducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const product = yield database_1.default.query('SELECT products.id_product, products.name_product, products.img_product, products.price_product FROM categories INNER JOIN products ON categories.id_category=products.id_category WHERE products.id_category = ?', [id]);
            if (product.length > 0) {
                return res.json({ data: product });
            }
            res.status(404).json({ text: "The product doesn't exists " });
        });
    }
    getProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const product = yield database_1.default.query('SELECT * FROM products WHERE id_product = ?', [id]);
            if (product.length > 0) {
                return res.json({ data: product });
            }
            res.status(404).json({ text: "The product doesn't exists " });
        });
    }
    getExtra(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const extra = yield database_1.default.query('SELECT * FROM extra INNER JOIN products ON extra.id_extra=products.id_extra WHERE products.id_product = ?', [id]);
            if (extra.length > 0) {
                return res.json({ data: extra });
            }
            res.status(404).json({ text: "The extras doesn't exists " });
        });
    }
    getProductIngredientPrincipal(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const productIngredientPrincipal = yield database_1.default.query("SELECT * FROM product_ingredient INNER JOIN ingredients ON product_ingredient.id_ingredient=ingredients.id_ingredient WHERE type_ingredient = 'principal' AND id_product =  ?", [id]);
            if (productIngredientPrincipal.length > 0) {
                return res.json({ data: productIngredientPrincipal });
            }
            res.status(404).json({ text: "The ingredient principal doesn't exists " });
        });
    }
}
const burguersController = new BurguersController();
exports.default = burguersController;
