"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const burguersController_1 = __importDefault(require("../controllers/burguersController"));
class BurguersRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', burguersController_1.default.list);
        this.router.get('/:id', burguersController_1.default.getProducts);
        this.router.get('/product/:id', burguersController_1.default.getProduct);
        this.router.get('/product_extra/:id', burguersController_1.default.getExtra);
        this.router.get('/product_ingredientPrincipal/:id', burguersController_1.default.getProductIngredientPrincipal);
        // this.router.get('/:id', burguersController.getOne);
        // this.router.post('/', burguersController.create);
        // this.router.put('/:id', burguersController.update);
        // this.router.delete('/:id', burguersController.delete);
    }
}
const burguersRoutes = new BurguersRoutes();
exports.default = burguersRoutes.router;
