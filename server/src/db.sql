Abre una línea de comandos (un terminal). Ve a la ruta C: /xampp /mysql /bin /  y ejecuta ese mysql.exe de esta manera:

mysql.exe –h localhost –u root -p


********************************************************


create database db_burguers;
use db_burguers;

create table categories(
id_category int (5)primary key,
name_category  varchar (255),
img_category  varchar (255))engine=innodb;

create table products(
id_product int (5)primary key,
name_product  varchar (255),
img_product  varchar (255),
price_product  varchar (255),
id_extras  enum ('0','1'),
id_category int (5),
id_extra int (5))engine=innodb;

create table extra(
id_extra int (5)primary key,
name_extra varchar (255),
img_extra varchar (255),
price_extra int (11),
state_extra enum ('inactive','active'))engine=innodb;

create table ingredients(
id_ingredient int (5)primary key,
name_ingredient  varchar (255),
img_ingredient  varchar (255),
price_ingredient  varchar (255),
state_ingredient  enum ('same','remove', 'extra'),
selected_ingredient enum('true', 'false'),
type_ingredient enum('principal', 'secondary')engine=innodb;

create table product_ingredient(
id_product int (5),
id_ingredient int (5))engine=innodb;


create table orders(
id_order int (5)primary key,
name_product  varchar (255),
img_product  varchar (255),
price_product  varchar (255),
id_extras  int (5))engine=innodb;


create table details_order(
id_order int (5)primary key,
name_product  varchar (255),
img_product  varchar (255),
price_product  varchar (255),
id_extras  int (5))engine=innodb;

create table ingredients(
-- id_order int (5)primary key,
bacon enum ('same','remove', 'extras'),
name_product  varchar (255),
img_product  varchar (255),
price_product  varchar (255),
id_extras  int (5))engine=innodb;

alter table products add foreign key (id_category) references categories (id_category) on update cascade on delete cascade;
alter table products add foreign key (id_extra) references extra (id_extra) on update cascade on delete cascade;
alter table product_ingredient add foreign key (id_product) references products (id_product) on update cascade on delete cascade;
alter table product_ingredient add foreign key (id_ingredient) references ingredients (id_ingredient) on update cascade on delete cascade;




insert into categories values(1, "Menús a la Parrilla", "https://static.vecteezy.com/system/resources/previews/008/507/708/original/classic-cheeseburger-with-beef-patty-pickles-cheese-tomato-onion-lettuce-and-ketchup-mustard-free-png.png");
insert into categories values(2, "Ensaladas", "https://s3.eu-central-1.amazonaws.com/www.burgerking.com.mx/wp-content/uploads/sites/3/2021/02/24181948/burgerking-ensalada-hawaiiana.png");
insert into categories values(3, "Postres", "https://www.soycorredor.es/uploads/s1/45/49/86/5cb81cf50de694856c8b46d9-postres-sin-azucar-3-recetas-para-introducirlos-en-tu-dieta-nzm.jpeg");
insert into categories values(4, "Complementos", "https://hamburguesasgarfields.com/wp-content/uploads/2018/01/French-Fries-french-fries-1-1.jpg");
insert into categories values(5, "Bebidas y Café", "https://tgrill.com.mx/tienda-en-linea/wp-content/uploads/2021/07/refresco-600-Mil.jpg");


insert into products values(1, "The Queen bacon menú grande", "https://e1.pngegg.com/pngimages/249/784/png-clipart-burger-cheese-burger.png", 20, 1, 1 );
insert into products values(2, "The Queen huevo menú grande", "https://www.pngfind.com/pngs/m/426-4269881_hamburguesa-png-transparent-png.png", 25, 1, 1 );
insert into products values(3, "Phopper menú grande", "https://www.pngfind.com/pngs/m/626-6263468_hamburguesa-bacon-clasica-hamburguesa-imagenes-de-hamburguesas-png.png", 40, 1, 1 );
insert into products values(4, "Hamburguesa de doritos", "https://www.seekpng.com/png/detail/701-7014065_hamburguesa-bembos-la-golf.png", 40, 1, 1 );
insert into products values(5, "Ensalada individual", "https://www.recetasnestle.com.mx/sites/default/files/srh_recipes/ae0d850f85f5af98ed0902bfb9a349a4.png", 20, 2, 2 );
insert into products values(6, "Helado de kitkat", "https://mcdonalds.es/api/cms/images/mcdonalds-es/6e4e7154-4515-412c-b6d7-96c3a7311248_8f660d847e7cca448dea2c44d4e7dbf7.png?auto=compress,format", 25, 3, 3 );
insert into products values(7, "Helado de oreo", "https://www.fiorence.com.mx/4918-large_default/pastel-de-oreo.jpg", 40, 3, 3 );
insert into products values(8, "Patatas clásicas grandes", "https://static8.depositphotos.com/1086145/935/i/950/depositphotos_9353524-stock-photo-french-fries-in-white-packaging.jpg", 40, 4, 4 );
insert into products values(9, "Queen Aros de cebolla", "https://estag.fimagenes.com/img/4/2/F/x/z/2Fxz_900.jpg", 40, 4, 4 );
insert into products values(10, "Cafe", "https://static.vecteezy.com/system/resources/previews/009/887/134/non_2x/cup-of-coffee-free-png.png", 40, 5, 5 );
insert into products values(11, "Coca cola grande", "https://cdn.myshoptet.com/usr/www.stramis.cz/user/shop/big/73999_coca-cola-1l.png?62b713d0", 40, 5, 5 );
insert into products values(12, "Fanta grande", "https://w7.pngwing.com/pngs/163/198/png-transparent-orange-juice-orange-drink-orange-soft-drink-glass-bottle-cola-fanta-glass-orange-orange-drink.png", 40, 5, 5 );
insert into products values(13, "Fanta limón grande", "https://ardiaprod.vtexassets.com/arquivos/ids/218884/Gaseosa-Fanta-Limon-Dulce-225-Lts-_1.jpg?v=637775797525400000", 40, 5, 5);

insert into extra values(1, "Sin extra", "", 0, 'inactive');
insert into extra values(2, "¿Hazlo Gigante?", "https://tgrill.com.mx/tienda-en-linea/wp-content/uploads/2021/07/refresco-600-Mil.jpg", 20, 'inactive');

insert into ingredients values(1, "Bacon", "https://www.pngplay.com/wp-content/uploads/6/Bacon-PNG-HD-Quality.png", 10, 'same', 'false');
insert into ingredients values(2, "Queso", "https://cdn2.cocinadelirante.com/sites/default/files/images/2020/10/queso-amarillo-hongvo.jpg", 10, 'same', 'false');
insert into ingredients values(3, "Carne Phopper", "https://cdn.shopify.com/s/files/1/0262/5424/5947/products/Carneparahamburguesa.png?v=1657403798", 10, 'same', 'false');
insert into ingredients values(4, "Lechuga", "https://png.pngtree.com/png-vector/20200907/ourlarge/pngtree-stacked-lettuce-leaves-png-image_2338294.jpg", 10, 'same', 'false');
insert into ingredients values(5, "Tomate", "https://www.kindpng.com/picc/m/255-2550145_tomates-cerises-sm-tomato-png-transparent-png.png", 10, 'same', 'false');
insert into ingredients values(6, "Cebolla", "https://www.pngitem.com/pimgs/m/51-512002_white-onion-cebolla-blanca-png-transparent-png.png", 10, 'same', 'false');
insert into ingredients values(7, "Mayonesa", "https://cdn0.recetasgratis.net/es/posts/2/0/8/thermomix_mayonesa_30802_orig.jpg", 10, 'same', 'false');
insert into ingredients values(8, "Salsa BBQ", "https://media.istockphoto.com/id/680114220/es/foto/salsa-parrilla-sobre-fondo-blanco.jpg?s=612x612&w=0&k=20&c=SqANzMhPlb5qWqDliBVIi6Go-u4mdumB0Mx1bzF-oiE=", 10, 'same', 'false');

insert into product_ingredient values(1, 1);
insert into product_ingredient values(1, 2);
insert into product_ingredient values(1, 3);
insert into product_ingredient values(1, 4);
insert into product_ingredient values(1, 5);
insert into product_ingredient values(1, 6);
insert into product_ingredient values(1, 7);
insert into product_ingredient values(2, 8);
insert into product_ingredient values(2, 1);
insert into product_ingredient values(2, 2);
insert into product_ingredient values(2, 3);
insert into product_ingredient values(2, 4);
insert into product_ingredient values(2, 5);
insert into product_ingredient values(2, 6);
insert into product_ingredient values(2, 7);
insert into product_ingredient values(2, 8);
insert into product_ingredient values(3, 1);
insert into product_ingredient values(3, 2);
insert into product_ingredient values(3, 3);
insert into product_ingredient values(3, 4);
insert into product_ingredient values(3, 5);
insert into product_ingredient values(3, 6);
insert into product_ingredient values(3, 7);
insert into product_ingredient values(3, 8);
insert into product_ingredient values(4, 1);
insert into product_ingredient values(4, 2);
insert into product_ingredient values(4, 3);
insert into product_ingredient values(4, 4);
insert into product_ingredient values(4, 5);
insert into product_ingredient values(4, 6);
insert into product_ingredient values(4, 7);
insert into product_ingredient values(4, 8);



SELECT products.name_product FROM categories INNER JOIN products ON categories.id_category=products.id_category;

SELECT * FROM products INNER JOIN categories ON products.id_category=categories.id_category;

SELECT * FROM products WHERE id_product = 1;

****************************************************************************************

SELECT products.id_product, products.name_product, products.img_product, products.price_product FROM categories INNER JOIN products ON categories.id_category=products.id_category WHERE products.id_category = 1;


SELECT * FROM categories INNER JOIN products ON categories.id_category=products.id_category WHERE products.id_category = 1;


SELECT * FROM extra INNER JOIN products ON extra.id_extra=products.id_extra WHERE products.id_product = 1;

SELECT * FROM product_ingredient INNER JOIN products ON product_ingredient.id_product=products.id_product WHERE product_ingredient.id_product = 1;




SELECT * FROM product_ingredient INNER JOIN ingredients ON product_ingredient.id_ingredient=ingredients.id_ingredient WHERE product_ingredient.id_product = 1;

SELECT * FROM product_ingredient INNER JOIN ingredients ON product_ingredient.id_ingredient=ingredients.id_ingredient WHERE product_ingredient.id_product = 1 AND type_ingredient = 'principal';

SELECT * FROM product_ingredient INNER JOIN ingredients ON product_ingredient.id_ingredient=ingredients.id_ingredient WHERE type_ingredient = 'principal' AND id_product = 1;

SELECT product_ingredient.id_product FROM product_ingredient;





















ALTER TABLE products
ADD id_extra int (5);