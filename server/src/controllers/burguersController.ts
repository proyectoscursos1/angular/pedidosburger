import { json, Request, Response } from 'express';

import pool from '../database';

class BurguersController {

    public async list(req: Request, res: Response) {
        const burguers = await pool.query('SELECT * FROM categories');
        res.json(burguers)
    }

    public async getProducts(req: Request, res: Response) {
        const { id } = req.params;
        const product = await pool.query('SELECT products.id_product, products.name_product, products.img_product, products.price_product FROM categories INNER JOIN products ON categories.id_category=products.id_category WHERE products.id_category = ?', [id]);
        if (product.length > 0) {
            return res.json({data: product});
        }
        res.status(404).json({ text: "The product doesn't exists " })
    }

    public async getProduct(req: Request, res: Response) {
        const { id } = req.params;
        const product = await pool.query('SELECT * FROM products WHERE id_product = ?', [id]);
        if (product.length > 0) {
            return res.json({data: product});
        }
        res.status(404).json({ text: "The product doesn't exists " })
    }

    public async getExtra(req: Request, res: Response) {
        const { id } = req.params;
        const extra = await pool.query('SELECT * FROM extra INNER JOIN products ON extra.id_extra=products.id_extra WHERE products.id_product = ?', [id]);
        if (extra.length > 0) {
            return res.json({data: extra});
        }
        res.status(404).json({ text: "The extras doesn't exists " })
    }

    public async getProductIngredientPrincipal(req: Request, res: Response) {
        const { id } = req.params;
        const productIngredientPrincipal = await pool.query("SELECT * FROM product_ingredient INNER JOIN ingredients ON product_ingredient.id_ingredient=ingredients.id_ingredient WHERE type_ingredient = 'principal' AND id_product =  ?", [id]);
        if (productIngredientPrincipal.length > 0) {
            return res.json({data: productIngredientPrincipal});
        }
        res.status(404).json({ text: "The ingredient principal doesn't exists " })
    }

    // public async getOne(req: Request, res: Response) {
    //     const { id } = req.params;
    //     const games = await pool.query('SELECT * FROM games WHERE id = ?', [id]);
    //     if (games.length > 0) {
    //         return res.json(games[0]);
    //     }
    //     res.status(404).json({ text: "The game doesn't exists " })
    // }

    // public async create(req: Request, res: Response): Promise<void> {
    //     await pool.query('INSERT INTO games set ?', [req.body]);
    //     res.json({ message: 'Game Saved' });
    // }

    // public async delete(req: Request, res: Response) {
    //     const { id } = req.params;
    //     await pool.query('DELETE FROM games WHERE id = ?', [id]);
    //     res.json({message: 'The game was deleted '});
    // }

    // public async update(req: Request, res: Response): Promise<void> {
    //     const { id } = req.params;
    //     await pool.query('UPDATE games set ? WHERE id = ?', [req.body, id]);
    //     res.json({message: 'The game was updated'});
    // }
}


const burguersController = new BurguersController();
export default burguersController;