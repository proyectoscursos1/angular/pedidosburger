import { Router } from "express";

import burguersController from "../controllers/burguersController";

class BurguersRoutes {
    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', burguersController.list);
        this.router.get('/:id', burguersController.getProducts);
        this.router.get('/product/:id', burguersController.getProduct);
        this.router.get('/product_extra/:id', burguersController.getExtra);
        this.router.get('/product_ingredientPrincipal/:id', burguersController.getProductIngredientPrincipal);
        // this.router.get('/:id', burguersController.getOne);
        // this.router.post('/', burguersController.create);
        // this.router.put('/:id', burguersController.update);
        // this.router.delete('/:id', burguersController.delete);
    }
}

const burguersRoutes = new BurguersRoutes();
export default burguersRoutes.router;